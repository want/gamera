#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

PY3VERS	:= $(shell py3versions -vr)
PY3VER	:= $(shell python3 -c 'import re, sys;print(re.match("^\d*\.\d*", sys.version).group())')

include /usr/share/python3/python.mk

include /usr/share/dpkg/architecture.mk
ifneq ($(DEB_BUILD_MULTIARCH),$(DEB_HOST_MULTIARCH))
  SET_CROSS_ENV = PYTHONPATH=/usr/lib/python$$pv/plat-$(DEB_HOST_MULTIARCH)

  # FIXME: is this sufficient?
  SET_CROSS_ENV += _PYTHON_HOST_PLATFORM=$(DEB_HOST_GNU_CPU)
endif

build-arch:
build-indep:
build-arch: build
build-indep: build
build: build-stamp

build-stamp: $(PY3VERS:%=build-stamp-python%) $(PY3VERS:%=check-stamp-python%)
	touch $@
build-stamp-python%:
	$(SET_CROSS_ENV) python$* setup.py build
	touch $@

check-stamp-python%:
ifeq (0,1)
ifeq ($(DEB_BUILD_MULTIARCH),$(DEB_HOST_MULTIARCH))
	rm -rf tmp
	mkdir -p tmp
endif
endif
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f *-stamp*
	rm -rf compile build
	find -name '*.py[co]' -exec rm -f {} \;
	find -type d -name __pycache__ | xargs -r rm -rf
	rm -rf tmp
	rm -rf docs/_build
	dh_clean

install: build install-prereq $(PY3VERS:%=install3-python%)
	dh_installdocs -ppython3-gamera ACKNOWLEDGEMENTS CHANGES KNOWN_BUGS LICENSE README.md TODO
#	pngtopnm < $(CURDIR)/debian/python3-gamera/pixmaps/icon.png | ppmtoxpm > $(CURDIR)/debian/python3-gamera/usr/share/pixmaps/gamera-gui.xpm
	: # Replace all '#!' calls to python with /usr/bin/python
	: # and make them executable
	for i in `find debian/python3-gamera* -mindepth 3 -type f`; do \
	  sed '1s,#!.*python[^ ]*\(.*\),#! /usr/bin/python3\1,' \
		$$i > $$i.temp; \
	  if cmp --quiet $$i $$i.temp; then \
	    rm -f $$i.temp; \
	  else \
	    mv -f $$i.temp $$i; \
	    chmod 755 $$i; \
	    echo "fixed interpreter: $$i"; \
	  fi; \
	done

install-prereq:
	dh_prep

install3-python%:
	$(SET_CROSS_ENV) python$* setup.py install \
		--root $(CURDIR)/debian/python3-gamera \
		$(py_setup_install_args)

	if `python$* -c "import sys; sys.exit(sys.version_info < (3,5))"`; \
	then \
		abitag=.$$(python$* -c "import sysconfig; print(sysconfig.get_config_var('SOABI'))"); \
	else \
		abitag=.$$(python$* -c "import sysconfig; print(sysconfig.get_config_var('SOABI'))")-$(DEB_HOST_MULTIARCH); \
	fi; \
	echo $$abitag;

# Build architecture-independent files here.
# Pass -i to all debhelper commands in this target to reduce clutter.
binary-indep: build install
	dh_python3 -i
	dh_link -i
	dh_compress -i -X.py
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

# Build architecture-dependent files here.
binary-arch: build install
	dh_python3 -a
	dh_strip -a
	dh_link -a
	dh_compress -a -X.py
	dh_fixperms -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
